//
//  SeriesCell.swift
//  anime
//
//  Created by Administrador on 21/06/18.
//  Copyright © 2018 alex. All rights reserved.
//

import UIKit

class SeriesCell: UICollectionViewCell {

    var setAttributes: Attributes?{
        didSet {
            setupCell()
        }
    }
    
    // MARK: - Let-Var
    @IBOutlet weak var serieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: SETUPS/LOADERS
    
    func setupCell() {
        
        guard let data = setAttributes else{
            print("Issue in dataResponse")
            return
        }
        
        serieImageView.sd_setImage(with: URL(string: (data.posterImage?.original!)!), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        
        titleLabel.text = data.slug
        
    }
}
