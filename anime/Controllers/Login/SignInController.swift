//
//  SignInController.swift
//  MyShot
//
//  Created by Alexander on 17/05/18.
//  Copyright © 2018 avalogics. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit
import NVActivityIndicatorView
import LTMorphingLabel

class SignInController: UIViewController, NVActivityIndicatorViewable, LTMorphingLabelDelegate{
    // MARK: - Let-Var
    
        //--Facebook
        var facebookUser: FacebookUser?
        let loginManager = LoginManager()
        let readPermissions: [ReadPermission] = [ .publicProfile, .email, .userFriends, .custom("user_posts")]
        //--Facebook
    
    let defaults = UserDefaults.standard
    let size = CGSize(width: 30, height: 30)
 
    
    var isTextOne = true
    var textOne: String = "¿Listo para ver anime?"
    var textTwo: String = "Disfruta con nosotros 😉"
    
    // MARK: - Outlets
    
    
    @IBOutlet weak var changeLabel: LTMorphingLabel!
    
    // MARK: - LifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setting up general actions/delegates/Core
        setUpActions()
        changeLabel.delegate = self
        changeLabel.morphingDuration = 1
        //Repeating text
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(toggleText), userInfo: nil, repeats: true)
    
        UserSingleton.shared.facebookSignOut()
        
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "SignInToSignUpController" {
//            
//            //let nav = segue.destination as! UINavigationController
//            
//        }
    }
    
    // MARK: - SetUps / Funcs
    
    func setUpUI(){}
    
    func setUpActions(){
        //Getting updated From Facebook Token
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
    }
    
    
    
    //Sign in Facebook actions and getting Graph data
    @IBAction func signinFacebook(_ sender: UIButton) {
        weak var weakSelf = self
        guard let weak = weakSelf else{return}
            loginManager.logIn(readPermissions: readPermissions, viewController: self) {
                loginResult in
                
                    guard case .success = loginResult else { print("Error")
                        return }
                
                    guard let accessToken = AccessToken.current else { return }
                
                    let facebookAPIManager = FacebookAPIManager(accessToken: accessToken)
                
                    facebookAPIManager.requestFacebookUser {
                            facebookUser in
                        weak.facebookUser = facebookUser
                        
                        
                        guard let token = accessToken.userId else{return}
                        
                        guard let firstName = facebookUser.firstName, let email = facebookUser.email else{return}
                        let userPhoto = "http://graph.facebook.com/\(token)/picture?type=large"
                        UserSingleton.shared.setCurrentUser(firstName, userPhoto, token)
                        UserSingleton.shared.keepLogged(Constants.DefaultKeys.logged)
                        weak.customLoading(at: self)
                        weak.performSegue(withIdentifier: Constants.Segues.signInToSideController, sender: self)
                        
                    }
            }
    }
    

   //ParseDurantion
    func parseDuration(timeString: String) -> TimeInterval {
        
        var interval: Double = 0
        let parts = timeString.components(separatedBy: ":")
        
        guard !timeString.isEmpty else { return 0 }
        
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }
        
        return interval
    }
    
    //Custom loading
    func customLoading(at:SignInController){
        at.startAnimating(size, message: "...", type: NVActivityIndicatorType.orbit)
    }
    
    // MARK: - Objective C
    @objc func toggleText() {
        weak var weakSelf = self
        guard let weak = weakSelf else{return}
        weak.changeLabel.text = isTextOne ? textTwo:textOne
        isTextOne = !isTextOne
    }
    
    
}

extension SignInController {
    
    func morphingDidStart(_ label: LTMorphingLabel) {
        
    }
    
    func morphingDidComplete(_ label: LTMorphingLabel) {
        
    }
    
    func morphingOnProgress(_ label: LTMorphingLabel, progress: Float) {
        
    }
    
}

