//
//  DetailController.swift
//  anime
//
//  Created by Administrador on 22/06/18.
//  Copyright © 2018 alex. All rights reserved.
//

import UIKit
import Hero
import Cosmos
import TagListView
import HexColors
import YouTubePlayer
import NVActivityIndicatorView

class DetailController: UIViewController, NVActivityIndicatorViewable {
  
    // MARK: - Let-Var
    var animeAttributes:Attributes?
    let size = CGSize(width: 30, height: 30)
    // MARK: - Outlets
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var raitingView: CosmosView!
    @IBOutlet weak var synopsisTextView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var showTags: TagListView!
    @IBOutlet weak var videoPlayer: YouTubePlayerView!
    
    // MARK: - LifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // setting up general actions/delegates/Core
        setUpActions()
        
        // setting up UI elements to be used through the code
        setUpUI()
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {}
 
    // MARK: - SetUps / Funcs
    func setUpUI(){}
    
    func setUpActions(){
        self.hero.isEnabled = true
        self.hero.modalAnimationType = .fade
        
        //Weak calls
        weak var weakSelf = self
        guard let weak = weakSelf else{return}
        
        //Filling fields
        if let banner = animeAttributes?.coverImage?.original, banner.isEmpty  {
          bannerImageView.sd_setImage(with: URL(string: banner), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }else{
            if let y = self.animeAttributes?.posterImage?.original{
                bannerImageView.sd_setImage(with: URL(string: y), placeholderImage: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        guard let slug = animeAttributes?.slug, let synopsis = animeAttributes?.synopsis else {return}
        
        guard let startDate = animeAttributes?.startDate, let endDate = animeAttributes?.endDate else { return }
        
        if let rating = animeAttributes?.ratingRank {
            raitingView.rating = Double(rating)
            raitingView.settings.updateOnTouch = false
        }
        titleLabel.text = slug
        synopsisTextView.text = synopsis
        dateLabel.text = "Since: \(startDate) until: \(endDate)"
        
        if let abbreviatedTitles = animeAttributes?.abbreviatedTitles{
            
            if abbreviatedTitles.count > 0{
                for type in abbreviatedTitles{
                    weak.showTags.addTags([type])
                    weak.showTags.tagBackgroundColor = HexColor(Constants.Colors.mainColor)!
                }
            }else{
                weak.showTags.addTag("Sin nombre alternativo")
                weak.showTags.tagBackgroundColor = .red
            }
        }
        
        //Calling video player
        
        Core.shared.playVideo(at: videoPlayer, animeAttributes: animeAttributes)
        
        
    }
    
    private func gettingData(){}
    
    //Close
    @IBAction func closeController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    // MARK: - Objective C
    

}
