//
//  SeriesController+CollectionViewDelegate.swift
//  anime
//
//  Created by Administrador on 21/06/18.
//  Copyright © 2018 alex. All rights reserved.
//

import Foundation
import UIKit

extension SeriesController: UICollectionViewDelegate, UICollectionViewDataSource{
    //SeriesToDetailController
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count > 0 ? self.data.count : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = seriesCollectionView.dequeueReusableCell(withReuseIdentifier: "SeriesCell", for: indexPath) as? SeriesCell else { return UICollectionViewCell() }
        if data.count > 0{
            cell.setAttributes = self.data[indexPath.row].attributes
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if data.count > 0{
            animeAttributes = self.data[indexPath.row].attributes
            performSegue(withIdentifier:
               "SeriesToDetailController", sender: self)
        }
    }
    
    
}
