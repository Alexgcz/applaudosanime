//
//  SeriesController.swift
//  anime
//
//  Created by Administrador on 21/06/18.
//  Copyright © 2018 alex. All rights reserved.
//

import UIKit

class SeriesController: UIViewController {
    // MARK: - Let-Var
    var animeAttributes:Attributes?
    var data = [DataGet]()
    
    // MARK: - Outlets
    @IBOutlet weak var seriesCollectionView: UICollectionView!
    
    // MARK: - LifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // setting up general actions/delegates/Core
        setUpActions()
        
        // setting up UI elements to be used through the code
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        gettingData()
    }
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SeriesToDetailController"{
            
            let detailController = segue.destination as! DetailController
            detailController.animeAttributes = animeAttributes
        }
    }
    
    // MARK: - SetUps / Funcs
    func setUpUI(){}
    
    func setUpActions(){
        //Delegating collectionview
        seriesCollectionView.delegate = self
        seriesCollectionView.dataSource = self
        
        //setting cell
        //Set Cell Identifier
        
        Core.shared.registerCellCollection(at: seriesCollectionView, named: "SeriesCell")
        
    }
    
    private func gettingData(){
        weak var weakSelf = self
        guard let weak = weakSelf else{return}
        UsersManager().LoadBars {
            anime in
            guard let animeData = anime?.data else{return}
            weak.data = animeData
            if weak.data.count > 0{
                weak.seriesCollectionView.reloadData()
            }
        }
    }
    
    // MARK: - Objective C
    
    

}
