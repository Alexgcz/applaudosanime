import Foundation
import SideMenu
import HexColors
import Alamofire
import SDWebImage

class SideMenuTableController: UITableViewController {
    
    
    // MARK: - Let-Var
    var tableFooterView: UIView?
    let defaults = UserDefaults.standard
    
    // MARK: - Outlets
    @IBOutlet weak var profileImageview: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var firstselectedView: UIView!
     @IBOutlet weak var secondselectedView: UIView!
    @IBOutlet weak var thirdselectedView: UIView!
    @IBOutlet weak var forthselectedView: UIView!
    @IBOutlet weak var fifthselectedView: UIView!
    
    // MARK: - LifeCycles
    override func viewDidLayoutSubviews() {
        setUpUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // setting up UI elements to be used through the code
        
        guard let urlImage =  URL(string: UserSingleton.shared.loggedUser(key:
            Constants.DefaultKeys.userPhoto)) else {return}
        self.profileImageview.sd_setImage(with:  urlImage, placeholderImage: #imageLiteral(resourceName: "placeholder"))
        userNameLabel.text =  "\(UserSingleton.shared.loggedUser(key: Constants.DefaultKeys.userName))\("😎")"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUPActions()
        gettingData()
    }
    
    // MARK: - SetUps / Funcs
    
    func setUpUI(){
        
        //NavigationController
        Core.shared.generalNavigation(at: self)
        
        //Circle imageview
        profileImageview.roundedImage()
        
        // Removing extra cells
        self.tableView.separatorStyle = .none
        
        
        
    }
    
    
    func setUPActions(){
        // Refresh cell blur effect in case it changed
        tableView.reloadData()
        guard SideMenuManager.default.menuBlurEffectStyle == nil else {
            return
        }
        
    }
    
    //Parsing Data
    private func gettingData(){}
    
    //Sign Out
    
    @IBAction func singOut(_ sender: UIButton) {
        Core.shared.logoutNavigation(at:self)
    }
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! UITableViewVibrantCell
        cell.blurEffectStyle = SideMenuManager.default.menuBlurEffectStyle
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //State selected
        
    }
    
   
    
}
