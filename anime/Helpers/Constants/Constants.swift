//
//  Constants.swift
//  anime
//
//  Created by Administrador on 21/06/18.
//  Copyright © 2018 alex. All rights reserved.
//

import Foundation
import Foundation

struct Constants {
    
    struct Credentials {
        static let appClientID = "dd031b32d2f56c990b1425efe6c42ad847e7fe3ab46bf1299f05ecd856bdb7dd"
        static let appClientSecret = "54d7307928f63414defd96399fc31ba847961ceaecef3a5fd93144e960c0e151"
    }
    
    //Defining colors identifiers
    struct Colors {
        static let mainColor = "#006ABA"
    }
    
    //Defining segues identifiers
    struct Segues {
        
        static let launchToSignInController = "LaunchToSignInController"
        static let signInToSideController = "SignInToSideController"
        static let launchToMenuController = "LaunchToMenuController"
        
    }
    
    //Defining controllers identifiers
    struct Controllers {
        
    }
    
    //Defining defaultkeys identifiers
    struct DefaultKeys {
        //Default keys on singleton
        static let logged = "logged"
        static let userID = "userID"
        static let userName = "userName"
        static let userPhoto = "userPhoto"
    }
    
    //Defining messages identifiers
    struct Messages {
        
    }
    
    //Defining Error messages
    struct Errors {
        
    }
    
}
