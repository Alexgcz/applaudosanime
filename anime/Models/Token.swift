import Foundation

struct Token : Codable {
	let access_token : String?
	let created_at : Int?
	let expires_in : Int?
	let refresh_token : String?
	let scope : String?
	let token_type : String?

	enum CodingKeys: String, CodingKey {

		case access_token = "access_token"
		case created_at = "created_at"
		case expires_in = "expires_in"
		case refresh_token = "refresh_token"
		case scope = "scope"
		case token_type = "token_type"
	}
}
