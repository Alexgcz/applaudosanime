
import Foundation
struct Anime : Codable {
	let data : [DataGet]?
	let meta : Meta?
	let links : Links?

	enum CodingKeys: String, CodingKey {

		case data = "data"
		case meta = "meta"
		case links = "links"
	}

	

}
