

import Foundation
struct Attributes : Codable {
	let createdAt : String?
	let updatedAt : String?
	let slug : String?
	let synopsis : String?
	let coverImageTopOffset : Int?
	let titles : Titles?
	let canonicalTitle : String?
	let abbreviatedTitles : [String]?
	let averageRating : String?
	let ratingFrequencies : RatingFrequencies?
	let userCount : Int?
	let favoritesCount : Int?
	let startDate : String?
	let endDate : String?
	let nextRelease : String?
	let popularityRank : Int?
	let ratingRank : Int?
	let ageRating : String?
	let ageRatingGuide : String?
	let subtype : String?
	let status : String?
	let tba : String?
	let posterImage : PosterImage?
	let coverImage : CoverImage?
	let episodeCount : Int?
	let episodeLength : Int?
	let youtubeVideoId : String?
	let showType : String?
	let nsfw : Bool?

	enum CodingKeys: String, CodingKey {

		case createdAt = "createdAt"
		case updatedAt = "updatedAt"
		case slug = "slug"
		case synopsis = "synopsis"
		case coverImageTopOffset = "coverImageTopOffset"
		case titles = "titles"
		case canonicalTitle = "canonicalTitle"
		case abbreviatedTitles = "abbreviatedTitles"
		case averageRating = "averageRating"
		case ratingFrequencies = "ratingFrequencies"
		case userCount = "userCount"
		case favoritesCount = "favoritesCount"
		case startDate = "startDate"
		case endDate = "endDate"
		case nextRelease = "nextRelease"
		case popularityRank = "popularityRank"
		case ratingRank = "ratingRank"
		case ageRating = "ageRating"
		case ageRatingGuide = "ageRatingGuide"
		case subtype = "subtype"
		case status = "status"
		case tba = "tba"
		case posterImage = "posterImage"
		case coverImage = "coverImage"
		case episodeCount = "episodeCount"
		case episodeLength = "episodeLength"
		case youtubeVideoId = "youtubeVideoId"
		case showType = "showType"
		case nsfw = "nsfw"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
		slug = try values.decodeIfPresent(String.self, forKey: .slug)
		synopsis = try values.decodeIfPresent(String.self, forKey: .synopsis)
		coverImageTopOffset = try values.decodeIfPresent(Int.self, forKey: .coverImageTopOffset)
		titles = try values.decodeIfPresent(Titles.self, forKey: .titles)
		canonicalTitle = try values.decodeIfPresent(String.self, forKey: .canonicalTitle)
		abbreviatedTitles = try values.decodeIfPresent([String].self, forKey: .abbreviatedTitles)
		averageRating = try values.decodeIfPresent(String.self, forKey: .averageRating)
		ratingFrequencies = try values.decodeIfPresent(RatingFrequencies.self, forKey: .ratingFrequencies)
		userCount = try values.decodeIfPresent(Int.self, forKey: .userCount)
		favoritesCount = try values.decodeIfPresent(Int.self, forKey: .favoritesCount)
		startDate = try values.decodeIfPresent(String.self, forKey: .startDate)
		endDate = try values.decodeIfPresent(String.self, forKey: .endDate)
		nextRelease = try values.decodeIfPresent(String.self, forKey: .nextRelease)
		popularityRank = try values.decodeIfPresent(Int.self, forKey: .popularityRank)
		ratingRank = try values.decodeIfPresent(Int.self, forKey: .ratingRank)
		ageRating = try values.decodeIfPresent(String.self, forKey: .ageRating)
		ageRatingGuide = try values.decodeIfPresent(String.self, forKey: .ageRatingGuide)
		subtype = try values.decodeIfPresent(String.self, forKey: .subtype)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		tba = try values.decodeIfPresent(String.self, forKey: .tba)
		posterImage = try values.decodeIfPresent(PosterImage.self, forKey: .posterImage)
		coverImage = try values.decodeIfPresent(CoverImage.self, forKey: .coverImage)
		episodeCount = try values.decodeIfPresent(Int.self, forKey: .episodeCount)
		episodeLength = try values.decodeIfPresent(Int.self, forKey: .episodeLength)
		youtubeVideoId = try values.decodeIfPresent(String.self, forKey: .youtubeVideoId)
		showType = try values.decodeIfPresent(String.self, forKey: .showType)
		nsfw = try values.decodeIfPresent(Bool.self, forKey: .nsfw)
	}

}
