
import Foundation
struct DataGet : Codable {
	let id : String?
	let type : String?
	let links : Links?
	let attributes : Attributes?
	let relationships : Relationships?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case type = "type"
		case links = "links"
		case attributes = "attributes"
		case relationships = "relationships"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		links = try values.decodeIfPresent(Links.self, forKey: .links)
		attributes = try values.decodeIfPresent(Attributes.self, forKey: .attributes)
		relationships = try values.decodeIfPresent(Relationships.self, forKey: .relationships)
	}

}
