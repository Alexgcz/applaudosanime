

import Foundation
struct Relationships : Codable {
	let genres : Genres?
	let categories : Categories?
	let castings : Castings?
	let installments : Installments?
	let mappings : Mappings?
	let reviews : Reviews?
	let mediaRelationships : MediaRelationships?
	let episodes : Episodes?
	let streamingLinks : StreamingLinks?
	let animeProductions : AnimeProductions?
	let animeCharacters : AnimeCharacters?
	let animeStaff : AnimeStaff?

	enum CodingKeys: String, CodingKey {

		case genres = "genres"
		case categories = "categories"
		case castings = "castings"
		case installments = "installments"
		case mappings = "mappings"
		case reviews = "reviews"
		case mediaRelationships = "mediaRelationships"
		case episodes = "episodes"
		case streamingLinks = "streamingLinks"
		case animeProductions = "animeProductions"
		case animeCharacters = "animeCharacters"
		case animeStaff = "animeStaff"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		genres = try values.decodeIfPresent(Genres.self, forKey: .genres)
		categories = try values.decodeIfPresent(Categories.self, forKey: .categories)
		castings = try values.decodeIfPresent(Castings.self, forKey: .castings)
		installments = try values.decodeIfPresent(Installments.self, forKey: .installments)
		mappings = try values.decodeIfPresent(Mappings.self, forKey: .mappings)
		reviews = try values.decodeIfPresent(Reviews.self, forKey: .reviews)
		mediaRelationships = try values.decodeIfPresent(MediaRelationships.self, forKey: .mediaRelationships)
		episodes = try values.decodeIfPresent(Episodes.self, forKey: .episodes)
		streamingLinks = try values.decodeIfPresent(StreamingLinks.self, forKey: .streamingLinks)
		animeProductions = try values.decodeIfPresent(AnimeProductions.self, forKey: .animeProductions)
		animeCharacters = try values.decodeIfPresent(AnimeCharacters.self, forKey: .animeCharacters)
		animeStaff = try values.decodeIfPresent(AnimeStaff.self, forKey: .animeStaff)
	}

}
