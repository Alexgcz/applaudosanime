

import Foundation
struct StreamingLinks : Codable {
	let links : Links?

	enum CodingKeys: String, CodingKey {

		case links = "links"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		links = try values.decodeIfPresent(Links.self, forKey: .links)
	}

}
